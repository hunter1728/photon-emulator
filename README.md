# README #

This is a Visual Studio project to emulate the various possible behaviours of a photon, with an aim to demonstrate how Bell's inequality shows a locality violation but also conforms to experimental results.

I tried to make the guts of the code non-Microsoft so it could be wrapped in different front ends.

### Who do I talk to? ###

* hunter1728 at gmail.com