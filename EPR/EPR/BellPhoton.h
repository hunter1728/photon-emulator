#pragma once

#include <random>

#include "Photon.h"

class CBellPhoton : public CPhoton
{
public:
	CBellPhoton(std::mt19937 & mt);
	CBellPhoton(std::mt19937 & mt, std::shared_ptr<int> pAngle);

	bool CanPassFilter(int nAngle);

	static std::vector<PhotonPtr> GeneratePhotonArray(int nCount, std::mt19937 & mt);
	static PhotonPairVector GenerateEntangledPhotonArray(int nCount, std::mt19937 & mt);

private:
	std::shared_ptr<int> m_pAngle;
	std::mt19937 & m_mt;
};
