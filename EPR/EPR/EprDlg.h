
// EprDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include <string>
#include <vector>
#include "Photon.h"

// CEprDlg dialog
class CEprDlg : public CDialogEx
{
// Construction
public:
	CEprDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EPR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	void LogOutput(const std::string & sText, const std::string & sData);

	void TestBasicFilterFunctionality();
	void TestRandomFilterAnglePassFail(std::string sPhotonType, const std::vector<PhotonPtr> & Photons);
	void TestMultipleFilters(std::string sPhotonType, const std::vector<int> & FilterAngles, const std::vector<PhotonPtr> & Photons);
	void TestEntanglement(std::string sPhotonType, const PhotonPairVector & PhotonPairs);
	void TestBellsInequality(std::string sPhotonType, const PhotonPairVector & PhotonPairs);

public:
	afx_msg void OnBnClickedOk();
	CListCtrl m_Output;
};
