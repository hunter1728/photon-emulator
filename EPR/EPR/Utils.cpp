﻿#define _USE_MATH_DEFINES // for C++
#include <cmath>

#include <cstdlib>
#include <vector>
#include <random>

using std::vector;
using std::uniform_real_distribution;
using std::mt19937;
using std::string;
using std::pair;

#include "Utils.h"
#include "Photon.h"

// for trace
#include "afx.h"


//*************************************************************
// Look into openFOAM for any tools to model wave dynamics!
//*************************************************************



//*************************************************************
//*************************************************************
int DifferenceBetweenAngles(int nAngle1, int nAngle2)
{
	int nDif = std::abs(nAngle1 - nAngle2);
	if (nDif > 180)
	{
		return std::abs(nDif - 360);
	}

	return nDif;
}

//*************************************************************
// Shift an angle 90 degrees
//*************************************************************
int ShiftAngle90(int nAngle)
{
	if (nAngle > 90)
	{
		return nAngle - 90;
	}
	else
	{
		return nAngle + 90;
	}
}

//*************************************************************
// This function needs more high level explanation of why
//*************************************************************
bool WillPhotonPassFilter(int nFilterAngle, int nPhotonAngle, mt19937 & mt)
{
	// cos takes radians - and none of this stuff returns exact values

	// one radian is equal to 180/π degrees (https://en.wikipedia.org/wiki/Radian)
	auto nRadians = (DifferenceBetweenAngles(nFilterAngle, nPhotonAngle) * M_PI) / 180;

	// http://www.users.csbsju.edu/~frioux/polarize/POLAR-sup.pdf
	
	// According to quantum mechanics, the probability that a photon will pass through a polarizing filter is equal 
	// to cos2(θ), where θ is the angle between the photon’s polarization and the orientation of the filter.
	// http://www.faithfulscience.com/quantum-physics/fuzzy-photons.html
	
	// That's what they say - but I still don't know why:
	auto nFractionalProbability = std::pow(std::cos(nRadians), 2);

	// round it so we can make safe comparisons with the rounded rand
	auto nProbability = std::round(nFractionalProbability * 100);

	// Because upper bounds are exclusive, is this ever so slightly inclined away from 100?
	uniform_real_distribution<double> dist(0.0, 100.0);

	auto nRand = std::round(dist(mt));

//	if (nRand <= nProbability)	// can't be this because if probability is zero, then rand can be zero and you can get a positive response!
	if (nProbability == 100.0 || nRand < nProbability)
	{
//		TRACE2("Passed Filter: %d, Photon: %d ", nFilterAngle, nPhotonAngle);
//		TRACE2("Prob: %f, Rand: %f\n", nProbability, nRand);
		return true;
	}

//	TRACE2("Failed Filter: %d, Photon: %d ", nFilterAngle, nPhotonAngle);
//	TRACE2("Prob: %f, Rand: %f\n", nProbability, nRand);
	return false;
}

//************************************************************
//************************************************************
int GetCountOfRandomFilterPassingPhotons(const vector<PhotonPtr> & Photons)
{
	std::random_device rd;
	std::mt19937 mt(rd());

	// Check for the random photon orientation probability of passing through a random filter angle (should be about 50/50)
	// We can use 360 for second parm because:
	// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
	std::uniform_real_distribution<double> dist(0, 360.0);
	int nFilterAngle = (int)dist(mt);

	return GetCountOfFilterPassingPhotons(nFilterAngle, Photons);
}

//************************************************************
// Version which takes an array of photons
//************************************************************
int GetCountOfFilterPassingPhotons(int nFilterAngle, const vector<PhotonPtr> & Photons)
{
	int nPass = 0;

	for each (auto pPhoton in Photons)
	{
		if (pPhoton->CanPassFilter(nFilterAngle))
		{
			nPass++;
		}
	}

	return nPass;
}

//************************************************************
// Test what happens to photons as they pass through a series
// of filters (at given angles) in sequence.
//************************************************************
vector<int> GetPassCountThroughMultipleFilters(const vector<int> & FilterAngles, const vector<PhotonPtr> & Photons)
{
	vector<int> PassCounts;
	vector<PhotonPtr> AllPhotons = Photons;

	for each (auto nFilterAngle in FilterAngles)
	{
		vector<PhotonPtr> PassPhotons;
		for each (auto pPhoton in AllPhotons)
		{
			if (pPhoton->CanPassFilter(nFilterAngle))
			{
				PassPhotons.push_back(pPhoton);
			}
		}

		PassCounts.push_back((int)PassPhotons.size());

		AllPhotons = PassPhotons;
	}

	return PassCounts;
}

//************************************************************
// Given a specific filter angle, and photon polarity angle, and a number of
// photons to try: how many pass?
//************************************************************
int GetPassingPhotonCount(int nFilterAngle, int nPhotonAngle, int nCount)
{
	int nPass = 0;

	std::random_device rd;
	std::mt19937 mt(rd());

	// Report how many photons of the given angle would pass through a filter of the given angle
	for (int x = 0; x < nCount; x++)
	{

		if (WillPhotonPassFilter(nFilterAngle, nPhotonAngle, mt))
		{
			nPass++;
		}
	}

	return nPass;
}

//************************************************************
// This function tests entanglement by a vector of pairs of (purportedly) entagled photons
// and making sure they consistently pass (or fail)
// Note: It's possible that the behaviours should be asymetrical. 
// TODO: Look into the above and adjust tests accordingly if necessary
//************************************************************
int CountEntanglementFailures(const PhotonPairVector & Photons)
{
	std::random_device rd;
	std::mt19937 mt(rd());

	int nEntanglementFailures = 0;

	// Generate a random filter angle
	std::uniform_real_distribution<double> dist(0, 360.0);
	int nFilterAngle = (int)dist(mt);

	// Make sure entangled photons always behave in a symmetrical way
	for each (auto PhotonPair in Photons)
	{
		auto backup = PhotonPair;	// Why is this here? For debugging?
		auto FirstResult = PhotonPair.first->CanPassFilter(nFilterAngle);
		auto SecondResult = PhotonPair.second->CanPassFilter(nFilterAngle);

		if (FirstResult != SecondResult)
		{
			nEntanglementFailures++;
		}
	}

	return nEntanglementFailures;
}


//************************************************************
// Count how many times both entangled photos got a matching result
//************************************************************
int CountBellsTestMatches(const PhotonPairVector & Photons)
{
	std::random_device rd;
	std::mt19937 mt(rd());

	int nMatches = 0;

	std::uniform_real_distribution<double> dist(0, 360.0);

	for each (auto PhotonPair in Photons)
	{
		// Pick a starting angle of either 0, 120 or 240
		int nFilterAngle = (int)dist(mt);
		if (nFilterAngle >= 240)
			nFilterAngle = 240;
		else if(nFilterAngle >= 120)
			nFilterAngle = 120;
		else
			nFilterAngle = 0;

		auto FirstResult = PhotonPair.first->CanPassFilter(nFilterAngle);

		// Now we need to add or subtract 120 for the second test
		if ((int)dist(mt) < 180)	// 50/50 chance
		{
			nFilterAngle -= 120;
			if (nFilterAngle < 0)
				nFilterAngle = 240;
		}
		else
		{
			nFilterAngle += 120;
			if (nFilterAngle == 360)
				nFilterAngle = 0;
		}

		auto SecondResult = PhotonPair.second->CanPassFilter(nFilterAngle);
		
		if (FirstResult == SecondResult)
		{
			nMatches++;
		}
	}

	return nMatches;
}
