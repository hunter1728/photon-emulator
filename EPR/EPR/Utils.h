#pragma once

#include "Photon.h"

int DifferenceBetweenAngles(int nAngle1, int nAngle2);
int ShiftAngle90(int nAngle);
bool WillPhotonPassFilter(int nAngle, int nPhotonAngle, std::mt19937 & mt);

int GetPassingPhotonCount(int nFilterAngle, int nPhotonAngle, int nCount);
int GetCountOfFilterPassingPhotons(int nFilterAngle, const std::vector<PhotonPtr> & Photons);
std::vector<int> GetPassCountThroughMultipleFilters(const std::vector<int> & FilterAngles, const std::vector<PhotonPtr> & Photons);
int GetCountOfRandomFilterPassingPhotons(const std::vector<PhotonPtr> & Photons);
int CountEntanglementFailures(const PhotonPairVector & PhotonPairs);
int CountBellsTestMatches(const PhotonPairVector & PhotonPairs);

