#include "BellPhoton.h"

#include "Utils.h"

using std::vector;
using std::pair;
using std::uniform_real_distribution;
using std::mt19937;
using std::make_shared;
using std::shared_ptr;


//*************************************************************
// A Bell photon manages entanglement through 'spooky action at
// a distance' (which in C++ means a pointer to its entangled partner)
//*************************************************************
CBellPhoton::CBellPhoton(std::mt19937 & mt) : m_mt(mt)
{
	// We can use 360 for second parm because:
	// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
	std::uniform_real_distribution<double> dist(0, 360.0);

	// What does this line do again?
	m_pAngle.reset(new int((int)dist(m_mt)));
}


//*************************************************************
//*************************************************************
CBellPhoton::CBellPhoton(std::mt19937 & mt, shared_ptr<int> pAngle) : m_mt(mt)
{
	// An entangled photon can share the same actual 'data' as the other photon. Unlike Einsteins, there are not two
	// copies of hidden variables, but a shared copy of a single visible variable.
	m_pAngle = pAngle;
}

//*************************************************************
//*************************************************************
bool CBellPhoton::CanPassFilter(int nFilterAngle)
{
	bool bPass = WillPhotonPassFilter(nFilterAngle, *m_pAngle, m_mt);

	// In this case (pass or fail) we're instantly (spooky action at a distance) affecting all photons entangled with this one
	if (bPass)
	{
		*m_pAngle = nFilterAngle;	// ensures it now passes
	}
	else
	{
		// Ensure it will not pass in the future (for this angle)
		*m_pAngle = ShiftAngle90(nFilterAngle);
	}

	return bPass;
}

//*************************************************************
//*************************************************************
vector<PhotonPtr> CBellPhoton::GeneratePhotonArray(int nCount, mt19937 & mt)
{
	vector<PhotonPtr> vResult;

	for (int x = 0; x < nCount; x++)
	{
		vResult.push_back(make_shared<CBellPhoton>(mt));
	}

	return vResult;
}

//*************************************************************
//*************************************************************
PhotonPairVector CBellPhoton::GenerateEntangledPhotonArray(int nCount, mt19937 & mt)
{
	PhotonPairVector vResult;

	for (int x = 0; x < nCount; x++)
	{
		// We can use 360 for second parm because:
		// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
		uniform_real_distribution<double> dist(0, 360.0);

		// Create two photons with the same random angle - this is the 'shared' variable that can also be
		// understood as the single wave function for both particles
		shared_ptr<int> pAngle;
		pAngle.reset(new int((int)dist(mt)));

		vResult.push_back(std::make_pair(make_shared<CBellPhoton>(mt, pAngle), make_shared<CBellPhoton>(mt, pAngle)));
	}

	return vResult;
}