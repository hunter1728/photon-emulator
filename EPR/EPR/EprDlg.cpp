
// EprDlg.cpp : implementation file
//

#include "stdafx.h"

#include "EPR.h"
#include "EprDlg.h"
#include "afxdialogex.h"
#include "resource.h"

#include "EinsteinPhoton.h"
#include "NaivePhoton.h"
#include "BellPhoton.h"
#include "Utils.h"

#include <sstream>
using std::vector;
using std::stringstream;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CEprDlg::CEprDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_EPR_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEprDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OUTPUT, m_Output);
}

BEGIN_MESSAGE_MAP(CEprDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CEprDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CEprDlg message handlers

BOOL CEprDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	m_Output.InsertColumn(0, _T("Msg"), LVCFMT_LEFT, 300);
	m_Output.InsertColumn(1, _T("Data"), LVCFMT_LEFT, 300);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEprDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialogEx::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEprDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEprDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



//************************************************************
//************************************************************
void CEprDlg::OnBnClickedOk()
{
	std::random_device rd;
	std::mt19937 mt(rd());

	m_Output.DeleteAllItems();

	// Run some tests that just make sure our basic filter logic is correct
	TestBasicFilterFunctionality(); 
	
	// Basic filter tests with random photons
	auto Photons = CNaivePhoton::GeneratePhotonArray(1000, mt);
	TestRandomFilterAnglePassFail("Naive", Photons);

	Photons = CEinsteinPhoton::GeneratePhotonArray(1000, mt);
	TestRandomFilterAnglePassFail("Einstein", Photons);

	Photons = CBellPhoton::GeneratePhotonArray(1000, mt);
	TestRandomFilterAnglePassFail("Bell", Photons);

	LogOutput("", ""); // spacer



	// Multi-filter tests
	Photons = CNaivePhoton::GeneratePhotonArray(1000, mt);
	vector<int> FilterAngles{ 90,90,90 };
	TestMultipleFilters("Naive", FilterAngles, Photons);

	Photons = CNaivePhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,180,180 };
	TestMultipleFilters("Naive", FilterAngles, Photons);

	Photons = CNaivePhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,135,180 };
	TestMultipleFilters("Naive", FilterAngles, Photons);

	LogOutput("", ""); // spacer



	Photons = CEinsteinPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,90,90 };
	TestMultipleFilters("Einstein", FilterAngles, Photons);

	Photons = CEinsteinPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,180,180 };
	TestMultipleFilters("Einstein", FilterAngles, Photons);

	Photons = CEinsteinPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,135,180 };
	TestMultipleFilters("Einstein", FilterAngles, Photons);

	LogOutput("", ""); // spacer

	Photons = CBellPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,90,90 };
	TestMultipleFilters("Bell", FilterAngles, Photons);

	Photons = CBellPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,180,180 };
	TestMultipleFilters("Bell", FilterAngles, Photons);

	Photons = CBellPhoton::GeneratePhotonArray(1000, mt);
	FilterAngles = { 90,135,180 };
	TestMultipleFilters("Bell", FilterAngles, Photons);

	LogOutput("", ""); // spacer



	// Basic entanglement tests
	auto EntangledPhotons = CNaivePhoton::GenerateEntangledPhotonArray(1000, mt);
	TestEntanglement("Naive", EntangledPhotons);

	EntangledPhotons = CEinsteinPhoton::GenerateEntangledPhotonArray(1000, mt);
	TestEntanglement("Einstein", EntangledPhotons);

	EntangledPhotons = CBellPhoton::GenerateEntangledPhotonArray(1000, mt);
	TestEntanglement("Bell", EntangledPhotons);

	LogOutput("", ""); // spacer

	// So at this point - Einstein and Bell are in a dead heat - both are random and both can be entangled

	// Bell's inequality tests
	EntangledPhotons = CNaivePhoton::GenerateEntangledPhotonArray(1000, mt);
	TestBellsInequality("Naive", EntangledPhotons);

	EntangledPhotons = CEinsteinPhoton::GenerateEntangledPhotonArray(1000, mt);
	TestBellsInequality("Einstein", EntangledPhotons);

	EntangledPhotons = CBellPhoton::GenerateEntangledPhotonArray(1000, mt);
	TestBellsInequality("Bell", EntangledPhotons);
}

//************************************************************
// Needed temporarily until we consolidate all the test reporting code into this object
//************************************************************
void CEprDlg::LogOutput(const std::string & sText, const std::string & sData)
{
	int nIndex = m_Output.GetItemCount();
	nIndex = m_Output.InsertItem(nIndex, CString(sText.c_str()));
	m_Output.SetItem(nIndex, 1, LVIF_TEXT, CString(sData.c_str()), 0, 0, 0, 0);
	m_Output.Invalidate();
	m_Output.UpdateWindow();
}

//************************************************************
//************************************************************
void CEprDlg::TestRandomFilterAnglePassFail(std::string sPhotonType, const std::vector<PhotonPtr> & Photons)
{
	int nPassingPhotons = GetCountOfRandomFilterPassingPhotons(Photons);

	// Format the message
	std::stringstream sCategoryMsg;
	sCategoryMsg << sPhotonType << " randomness test:";

	std::stringstream sMsg;
	sMsg << nPassingPhotons << "/" << Photons.size();

	LogOutput(sCategoryMsg.str(), sMsg.str());
}

//************************************************************
//************************************************************
void CEprDlg::TestMultipleFilters(std::string sPhotonType, const vector<int> & FilterAngles, const vector<PhotonPtr> & Photons)
{
	auto PassCounts = GetPassCountThroughMultipleFilters(FilterAngles, Photons);

	// Format the message
	std::stringstream sCategoryMsg;
	sCategoryMsg << sPhotonType << " multi filters (";
	for each (auto nAngle in FilterAngles)
	{
		sCategoryMsg << nAngle << ",";
	}
	sCategoryMsg.seekp(-1, sCategoryMsg.cur); // shuffle pointer back one so we can overwrite the extra slash
	sCategoryMsg << ") 1000 initial photons:";


	std::stringstream sMsg;
	for each (auto nCount in PassCounts)
	{
		sMsg << nCount << "/";
	}
	sMsg.seekp(-1, sMsg.cur); // shuffle pointer back one so we can overwrite the extra slash
	sMsg << " ";// overwrite the last slash. A bit of a hack. Ideally should have an 'implode' function for these.
	
	LogOutput(sCategoryMsg.str(), sMsg.str());
}

//************************************************************
//************************************************************
void CEprDlg::TestBasicFilterFunctionality()
{
	int nPhotonCount = 10000;

	int nFilterAngle = 144;
	int nPhotonAngle = 54;
	int nPassCount = GetPassingPhotonCount(nFilterAngle, nPhotonAngle, nPhotonCount);

	stringstream sMsg;
	sMsg << nPassCount << "/" << nPhotonCount;
	LogOutput("Basic Filter Test (90 degrees, must fail):", sMsg.str());

	nFilterAngle = 200;
	nPhotonAngle = 245;
	nPassCount = GetPassingPhotonCount(nFilterAngle, nPhotonAngle, nPhotonCount);

	sMsg = stringstream();
	sMsg << nPassCount << "/" << nPhotonCount;
	LogOutput("Basic Filter Test (45 degrees, about 50/50):", sMsg.str());


	nFilterAngle = 80;
	nPhotonAngle = 80;
	nPassCount = GetPassingPhotonCount(nFilterAngle, nPhotonAngle, nPhotonCount);

	sMsg = stringstream();
	sMsg << nPassCount << "/" << nPhotonCount;
	LogOutput("Basic Filter Test (matching degrees, must pass):", sMsg.str());

	LogOutput("", ""); // spacer
}


//************************************************************
//************************************************************
void CEprDlg::TestEntanglement(std::string sPhotonType, const PhotonPairVector & PhotonPairs)
{
	int nEntanglementFailures = CountEntanglementFailures(PhotonPairs);

	// Format the message
	std::stringstream sCategoryMsg;
	sCategoryMsg << sPhotonType << " entanglement failures:";

	std::stringstream sMsg;
	sMsg << nEntanglementFailures << "/" << PhotonPairs.size();

	LogOutput(sCategoryMsg.str(), sMsg.str());
}


//************************************************************
//************************************************************
void CEprDlg::TestBellsInequality(std::string sPhotonType, const PhotonPairVector & PhotonPairs)
{
	int nMatches = CountBellsTestMatches(PhotonPairs);

	// Format the message
	std::stringstream sCategoryMsg;
	sCategoryMsg << sPhotonType << " Bell's Test:";

	std::stringstream sMsg;
	sMsg << "Matches: " << nMatches << "/" << PhotonPairs.size() << " (" << nMatches * 100 / PhotonPairs.size() << "%)";

	LogOutput(sCategoryMsg.str(), sMsg.str());
}
