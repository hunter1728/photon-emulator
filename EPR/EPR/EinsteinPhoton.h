#pragma once

#include <random>
#include <vector>

#include "Photon.h"

class CEinsteinPhoton : public CPhoton
{
public:
	CEinsteinPhoton(std::mt19937 & mt);
	CEinsteinPhoton(std::mt19937 & mt, std::vector<bool> vResults);

	// helpers
	static std::vector<PhotonPtr> GeneratePhotonArray(int nCount, std::mt19937 & mt);
	static PhotonPairVector GenerateEntangledPhotonArray(int nCount, std::mt19937 & mt);

	bool CanPassFilter(int nAngle);

private:
	// Einstein photons have hidden variables - the answers to everything that the photon could do
	// embedded within them
	std::vector<bool> m_vResults;
	std::mt19937 & m_mt;

	static std::vector<bool> BuildFilterResultsVector(int nAngle, std::mt19937 & mt);
};
