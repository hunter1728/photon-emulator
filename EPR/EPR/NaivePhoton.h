#pragma once

#include <random>
#include <memory>

#include "Photon.h"

class CNaivePhoton : public CPhoton
{
public:
	CNaivePhoton(std::mt19937 & mt);

	bool CanPassFilter(int nAngle);

	static std::vector<PhotonPtr> GeneratePhotonArray(int nCount, std::mt19937 & mt);
	static PhotonPairVector GenerateEntangledPhotonArray(int nCount, std::mt19937 & mt);


private:
	int m_nAngle;
	std::mt19937 & m_mt;
};
