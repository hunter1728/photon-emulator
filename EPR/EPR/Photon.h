#pragma once

#include <random>
#include <memory>


class CPhoton
{
public:
	CPhoton();
	virtual ~CPhoton() {};
	virtual bool CanPassFilter(int nAngle) = 0;
};

typedef std::shared_ptr<CPhoton> PhotonPtr;
typedef std::vector<std::pair<PhotonPtr, PhotonPtr>> PhotonPairVector;