#include "EinsteinPhoton.h"

#include "Utils.h"

using std::vector;
using std::pair;
using std::uniform_real_distribution;
using std::mt19937;
using std::make_shared;

//*************************************************************
// An 'Einstein' photon is similar to a naive photon, except
// that it has a hidden variable, which pre-determines how it
// will react to every filter angle it might encounter
// 
// This allows it to coordinate its (opposite) response with
// an entangled counterpart.
//*************************************************************
CEinsteinPhoton::CEinsteinPhoton(std::mt19937 & mt) :
m_mt(mt)
{
	// We can use 360 for second parm because:
	// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
	std::uniform_real_distribution<double> dist(0, 360.0);

	int nAngle = (int)dist(m_mt);

	// Compute the binary result it would yield at
	// all the possible filter angles. - In other words, determine the randomness up front.
	// This is the 'hidden variable' in an Einstein theory of the photon.  This allows it
	// to be entangled because it can coordinate with the other photon on the basis of an initial state

	m_vResults = BuildFilterResultsVector(nAngle, mt);
}


//*************************************************************
//*************************************************************
CEinsteinPhoton::CEinsteinPhoton(std::mt19937 & mt, std::vector<bool> vResults) :
	m_mt(mt)
{
	// Einstein photons allow the hidden variable to be provided on the constructor so that it can be shared with
	// entangled particles.

	// In a truer scenario, the results would be the opposite vector, as entanglement coordinate opposites, but the
	// math for this experiment is basically the same.
	m_vResults = vResults;
}

//*************************************************************
//*************************************************************
bool CEinsteinPhoton::CanPassFilter(int nFilterAngle)
{
	// this is just an optimization so that we can use a half-cicle for the results set
	if (nFilterAngle >= 180)
	{
		nFilterAngle -= 180;
	}

	// If we were meant to pass this filter (based on our 'hidden variable' stored in m_vResult) then rebuild
	// our hidden variables to now correspond to this new polatization angle. (which in turn ensures that it will
	// indefinitely pass through any subsequent filter at this exact angle)
	if (m_vResults[nFilterAngle])
	{
		m_vResults = BuildFilterResultsVector(nFilterAngle, m_mt);
		return true;
	}
	else
	{
		// If we were NOT meant to pass this filter (based on our 'hidden variable' stored in m_vResult) then rebuild
		// our hidden variables to now correspond to this new anti-polatization angle. (which in turn ensures that it will
		// indefinitely NOT pass through any subsequent filter at this exact angle)
		nFilterAngle = ShiftAngle90(nFilterAngle);

		m_vResults = BuildFilterResultsVector(nFilterAngle, m_mt);
		return false;
	}
}

//*************************************************************
//*************************************************************
vector<PhotonPtr> CEinsteinPhoton::GeneratePhotonArray(int nCount, mt19937 & mt)
{
	vector<PhotonPtr> vResult;

	for (int x = 0; x < nCount; x++)
	{
		vResult.push_back(make_shared<CEinsteinPhoton>(mt));
	}

	return vResult;
}


//*************************************************************
//*************************************************************
PhotonPairVector CEinsteinPhoton::GenerateEntangledPhotonArray(int nCount, mt19937 & mt)
{
	PhotonPairVector vResult;

	for (int x = 0; x < nCount; x++)
	{
		// We can use 360 for second parm because:
		// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
		uniform_real_distribution<double> dist(0, 360.0);

		// Use the same random angle and results sets for both photons
		int nAngle = (int)dist(mt);

		// vResult is our Einstain 'hidden variable', which ensures that the entabgled photos are symmetrical in their
		// behaviour. (Maybe it's supposed to be asymmetrical, but the net effect is the same)  When we create our pairs
		// of photons, make sure they share the same hidden variable.
		auto vResults = BuildFilterResultsVector(nAngle, mt);

		vResult.push_back(std::make_pair(make_shared<CEinsteinPhoton>(mt, vResults), make_shared<CEinsteinPhoton>(mt, vResults)));
	}

	return vResult;
}


//*************************************************************
// Helper function to prepopulate a results vector with 180 possible results
//*************************************************************
vector<bool> CEinsteinPhoton::BuildFilterResultsVector(int nAngle, mt19937 & mt)
{
	// Build a half-circle of options for this angle
	vector<bool> vResults(180);

	// We're buuilding the pre-decided behaviour for this photon on any angle. Certainly Einstein did not envision
	// it as an array of pre-decisions, but the net effect would be some kind of data and algorithm that would pre-decide
	// the result under any angle. So same dif.
	for (int nFutureFilterAngle = 0; nFutureFilterAngle < 180; nFutureFilterAngle++)
	{
		vResults[nFutureFilterAngle] = WillPhotonPassFilter(nFutureFilterAngle, nAngle, mt);
	}

	return vResults;
}