#include "NaivePhoton.h"

#include "Utils.h"

using std::vector;
using std::pair;
using std::uniform_real_distribution;
using std::mt19937;
using std::make_shared;

//*************************************************************
// A 'naive' theory of a photon is that it is created with an 
// initial polarization, and that's all there is to it.
// 
// The problem with this is that it should fail all entanglement
// experiments because there is no way to coordinate its behaviour with
// another photon. It can't coordinate a hidden variable, like an
// Einstein photon, not can it violate locality like a Bell photon.
//*************************************************************
CNaivePhoton::CNaivePhoton(std::mt19937 & mt) : m_mt(mt)
{
	// We can use 360 for second parm because:
	// The upper bound for random values, exclusive. (https://msdn.microsoft.com/library/5cf906fd-0319-4984-b21b-98425cd7532d.aspx#uniform_real_distribution__uniform_real_distribution)
	std::uniform_real_distribution<double> dist(0, 360.0);

	// Set the initial polarization angle randomly
	m_nAngle = (int)dist(m_mt);
}

//*************************************************************
//*************************************************************
bool CNaivePhoton::CanPassFilter(int nFilterAngle)
{
	// Because upper bounds are exclusive, is this ever so slightly inclined to the negative?
	std::uniform_real_distribution<double> dist(0, 1.0);

	// Naively assume that the pass/fail of the photon through the filter is just a 50/50 random chance.
	if((int)std::round(dist(m_mt)))
		return true;
	else
		return false;
}

//*************************************************************
//*************************************************************
vector<PhotonPtr> CNaivePhoton::GeneratePhotonArray(int nCount, mt19937 & mt)
{
	vector<PhotonPtr> vResult;

	for (int x = 0; x < nCount; x++)
	{
		vResult.push_back(make_shared<CNaivePhoton>(mt));
	}

	return vResult;
}

//*************************************************************
//*************************************************************
PhotonPairVector CNaivePhoton::GenerateEntangledPhotonArray(int nCount, mt19937 & mt)
{
	PhotonPairVector vResult;

	// Naive photons aren't really entangled - so there's nothing magical to do here.
	for (int x = 0; x < nCount; x++)
	{
		vResult.push_back(std::make_pair(make_shared<CNaivePhoton>(mt), make_shared<CNaivePhoton>(mt)));
	}

	return vResult;
}
